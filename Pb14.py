# -*- coding: utf-8 -*-
"""
Created on Wed Dec 19 17:48:56 2018

@author: lsterna
"""

def collapz_pb(number):
    res = number
    chaine = 0
    while(res != 1):
        if(res % 2 == 0):
            res = res /2
        else:
            res = res * 3 + 1
        chaine += 1 
    return chaine      
            

collapz_pb(17)

def force_brute():
    best = 0
    ind = -1
    for i in range(1,1*10**6+1):
        if(i % 10**5 == 0 ):
            print(i)
        new_challenger = collapz_pb(i)
        if(new_challenger > best):
            best = new_challenger
            ind = i
    return best,ind
        
best,ind = force_brute()