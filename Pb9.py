# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 17:20:42 2018

@author: lsterna
"""

def cond1(a,b,c):
    if(a<b and b<c):
        return True
    else:
        return False
    
def cond2(a,b,c):
    if((a*a + b*b) == c*c):
        return True
    else:
        return False

def cond3(a,b,c):
    if((a + b + c) == 1000):
        return True
def pythagore_triplet():    
    for a in range(1001):
        for b in range(a+1,1001- a):
            for c in range(b+1,1001 - b - a):
                if(cond2(a,b,c)):
                    if(cond3(a,b,c)):
                        return (a,b,c)
                
a,b,c = pythagore_triplet()  
print(a * b * c)
            
    