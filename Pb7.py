# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 11:30:14 2018

@author: lsterna
"""
from math import sqrt

num =10001

def premier(number):
    for i in range(2,int(sqrt(number))+1):
        if(number % i ==0):
            return False
    return True

cpt = 1
value = 3

while (cpt != num):
#    print("cpt : " + str(cpt))
#    print("value : " + str(value))
    if(premier(value)):
        cpt +=1
    value +=1

print(value - 1)
    
    