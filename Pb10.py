# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 11:29:31 2018

@author: lsterna
"""

from math import sqrt

def premier(number):
    for i in range(2,int(sqrt(number))+1):
        if(number % i ==0):
            return False
    return True

borne = 2000000

list_premier = []
for i in range(2,borne):
    if(premier(i)):
        list_premier.append(i)
        
cpt = 0 
for elem in list_premier:
    cpt += elem
    
print(cpt)