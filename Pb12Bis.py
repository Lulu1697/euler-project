# -*- coding: utf-8 -*-
"""
Created on Thu Dec 13 09:50:48 2018

@author: lsterna
"""
from math import sqrt,factorial
from itertools import combinations
import time


def premier(a):
    for i in range(2, int(sqrt(a)) + 1 ):
        if(a % i == 0):
            return False
    return True

#number=8*10**6

#def diviseur(nombre):
#    diviseur = []
#    for i in range(2,nombre):
#        if(nombre % i == 0):
#            diviseur.append(i)
#    return (len(diviseur) + 2),diviseur
#
#div = diviseur(90)

#def list_premier(number):
#    list_premier = []
#    for i in range(2,number):
#        if(premier(i)):
#            list_premier.append(i)
#    return list_premier
#
#lp = list_premier(number)

#with open('C:/Users/lulu/Desktop/euler-project/euler-project/premier.txt', 'w') as f:
#    for item in lp:
#        f.write("%s\n" % item)      
lp = []
with open('C:/Users/lulu/Desktop/euler-project/euler-project/premier.txt', 'r') as f:
    for line in f:
        tampon = line.replace('\n','')
        lp.append(int(tampon))    



def decomposition_fc_p(nombre):
    i = 0
    ldp = []
    lpuiss = []
    while(lp[i] < (int(nombre/2)+1)):
        if(nombre % lp[i] == 0):
            ldp.append(lp[i])
            cpt= 2
            while((nombre % lp[i]**cpt ==0)):
                cpt += 1
            lpuiss.append(cpt - 1)
        i +=1
    return ldp,lpuiss


def div_combinartion_simplifie(l1,l2,nombre):
#    val_max = int(nombre / 2)
    list_res = []
    cptf = 1
    n = len(l1)
    l_all = list(l1)
    for i in range(n):
        if(l2[i] != 1):
            j = 2 
            l_all.append(l1[i]**j)
            while(j < l2[i]):
                j += 1
                l_all.append(l1[i]**j)           
#    print(l_all)
    for i in range(1,n+1):
        perm = combinations(l_all,i)
        for item in list(perm):
#            print(item)
            diviseur = 1
            for value in item:
                diviseur *= value
            if(nombre % diviseur == 0):
                if(diviseur not in list_res):
#                    print(diviseur)
                    list_res.append(diviseur)
                    cptf += 1
#                    print("diviseur {} ajouté".format(diviseur))
    if(cptf == 1):
        return list([1,nombre])
    else:
        list_res.append(1)
        return (list_res)
            

def combinaison(l1,l2,nombre):
    res = []
    for eleml1 in l1:
        for eleml2 in l2:
            if(nombre %( eleml1 * eleml2) == 0):
                if((eleml1 * eleml2) not in res):
                    res.append(eleml1 * eleml2)
    return res 


def parcours_triangle():
#    sum_value = 1
    ajout = 2
    cpt = 0
    while(cpt < 500):
        sum_value = (ajout * (ajout +1)) /2
        l1,l2 = decomposition_fc_p(ajout)
        l_ajout = div_combinartion_simplifie(l1,l2,ajout)
        l1,l2 = decomposition_fc_p(ajout + 1)
        l_ajout1 = div_combinartion_simplifie(l1,l2,ajout + 1)
        res = combinaison(l_ajout,l_ajout1,sum_value)
        res.sort()
        cpt = len(res)
#        if(cpt not in all_cpt):
#            all_cpt.append(cpt)
#        print("cpt : " + str(cpt))
#        print("sum_value : " + str(sum_value))
#        print("ajout : " + str(ajout))
#        print()
#        sum_value += ajout
        ajout += 1
    return(sum_value)
start = time.time()

sum_value = int(parcours_triangle())

    
    