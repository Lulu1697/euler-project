# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 13:21:44 2018

@author: lsterna
"""
import numpy as np

f = open("C:/Users/lulu/Desktop/euler-project/euler-project/gridPb1.txt","r")

matrice = []
for line in f:
    tampon = line.split(" ")
    new_tampon_int = []
    for elem in tampon:
        print(elem)
        new_tampon_int.append(int(elem))
    matrice.append(new_tampon_int)
    
matrice = np.array(matrice)
ind_parcours = 4
n = matrice.shape[0]

valeur_max = 0
indi = 0
indj = 0
direction = ""
for i in range(n):
    for j in range(n):
        if((n - i) > ind_parcours):
            cpt = 1
            for k in range(i,i+ind_parcours):
                print("matrice[k][j] : " + str(matrice[k][j]))
                cpt *= matrice[k][j]
            print("cpt droite : " + str(cpt))
            if(cpt > valeur_max):
                valeur_max = cpt
                indi = i
                indj = j
                direction = "bas"
        if((n - j) > ind_parcours):
            cpt = 1
            for k in range(j,j+ind_parcours):
                cpt *= matrice[i][k]
            if(cpt > valeur_max):
                valeur_max = cpt
                indi = i
                indj = j
                direction = "droite"
        if(((n - i) > ind_parcours) and ((n - j) >ind_parcours)):
            cpt = 1
            for k in range(0,ind_parcours):
                cpt *= matrice[i+k][j+k]
            if(cpt > valeur_max):
                valeur_max = cpt
                indi = i
                indj = j
                direction = "diagonale_droite"
        if(((n - i) > ind_parcours) and (j >ind_parcours)):
            cpt = 1
            for k in range(0,ind_parcours):
                cpt *= matrice[i+k][j-k]
            if(cpt > valeur_max):
                valeur_max = cpt
                indi = i
                indj = j
                direction = "diagonale_gauche"
print("indi : {}, indj : {}, value: {}, direction : {}".format(indi,indj,valeur_max,direction))