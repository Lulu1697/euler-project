# -*- coding: utf-8 -*-
"""
Created on Mon Dec 10 13:40:38 2018

@author: lsterna
"""

fib1 = 1
fib2 = 2

cpt = 0

while(fib2 < 4*10**6):
    if(fib2 %2==0):
        cpt += fib2
    tampon = fib2
    fib2 = fib1+fib2
    fib1 = tampon

print(cpt)