# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 11:26:10 2018

@author: lsterna
"""

n = 100

def somme_des_carre(n):
    return ((n*(n+1)*(2*n+1))/6)

def carre_somme(n):
    return(((n*(n+1))/2)**2)
    
print(carre_somme(n) - somme_des_carre(n))